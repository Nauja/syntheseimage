#version 430    // core profile, compute shader

#ifdef COMPUTE_SHADER
#define M_PI 3.1415f

struct Ray
{
    vec3 o;
    vec3 d;
};

struct Sphere
{
    float rad;          // radius
    vec3 p, e, c;            // position, emission, color
};    

uniform int width; // Image width.
uniform int height; // Image height.
uniform int samps; // Number of samples.
shared Sphere spheres[9] = { //Scene: radius, position, emission, color, material
    { 2., vec3(50, 70, 51.6), vec3(150000, 150000, 150000), vec3(.999, .999, .999) }, //Light
    { 1e5, vec3(1e5 + 1, 40.8, 81.6), vec3(0, 0, 0), vec3(.75, .25, .25) },  //Left
    { 1e5, vec3(-1e5 + 99, 40.8, 81.6), vec3(0, 0, 0), vec3(.25, .25, .75) },    //Rght
    { 1e5, vec3(50, 40.8, 1e5), vec3(0, 0, 0), vec3(.75, .75, .25) },    //Back
    { 1e5, vec3(50, 40.8, -1e5 + 170), vec3(0, 0, 0), vec3(0, 0, 0) },  //Frnt
    { 1e5, vec3(50, 1e5, 81.6), vec3(0, 0, 0), vec3(.75, .75, .75) },    //Botm
    { 1e5, vec3(50, -1e5 + 81.6, 81.6), vec3(0, 0, 0), vec3(.75, .75, .75) },    //Top
    { 16.5, vec3(27, 16.5, 47), vec3(0, 0, 0), vec3(.999, .999, .999) }, //Mirr
    { 16.5, vec3(73, 16.5, 78), vec3(0, 0, 0), vec3(.999, .999, .999) }, //Glas
}; // Spheres.
uniform int spheresCount; // Number of spheres.
uniform vec3 light_pos;
uniform int light_index; // Index of the light sphere.
uniform vec3 cam_pos; // Camera position.
uniform vec3 cam_dir; // Camera direction.

// WARRING: works only if r.d is normalized
float is_intersecting (in const Ray r, in const Sphere s)
{
    vec3 op = s.p - r.o;
    float t;
    float b = dot (op, r.d);
    float det = b * b - dot (op, op) + s.rad * s.rad;
    if (det < 0)
        return 0;
    else
        det = sqrt (det);
    return (t = b - det) >= 0 ? t : ((t = b + det) >= 0 ? t : 0);
}

bool intersect (in const Ray r, out float t, out int id)
{
    float d;
    float inf = t = 1e20;
    for (int i = spheresCount; i >= 0; i--) {
        d = is_intersecting(r, spheres[i]);
        if (bool(d) && d < t)
        {
            t = d;
            id = i;
        }
    }
    return t < inf;
}

uint hash( uint x ) {
    x += ( x << 10u );
    x ^= ( x >>  6u );
    x += ( x <<  3u );
    x ^= ( x >> 11u );
    x += ( x << 15u );
    return x;
}

uint hash( in const uvec2 v ) { return hash( v.x ^ hash(v.y)                         ); }
uint hash( in const uvec3 v ) { return hash( v.x ^ hash(v.y) ^ hash(v.z)             ); }

float floatConstruct(uint m ) {
    const uint ieeeMantissa = 0x007FFFFFu; // binary32 mantissa bitmask
    const uint ieeeOne      = 0x3F800000u; // 1.0 in IEEE binary32

    m &= ieeeMantissa;                     // Keep only mantissa bits (fractional part)
    m |= ieeeOne;                          // Add fractional part to 1.0

    float  f = uintBitsToFloat( m );       // Range [1:2]
    return f - 1.0;                        // Range [0:1]
}

float random_u(in const vec3 v)
{
    return floatConstruct(hash(floatBitsToUint(v)));
}

vec3 get_light_point(in vec3 shading_point)
{
    Sphere lp = spheres[light_index];
    float r = lp.rad;
    float r1 = random_u(shading_point);
    float r2 = random_u(shading_point);

    float r1_2pi = 2.f * M_PI * r1;
    float r2_1sqrt = sqrt(r2 * (1.f - r2));
    return vec3(
        light_pos.x + 2.f * r * cos(r1_2pi) * r2_1sqrt,
        light_pos.y + 2.f * r * sin(r1_2pi) * r2_1sqrt,
        light_pos.z + r * (1.f - 2.f * r2)
    ); //Sphere
}

bool occlude (in const Ray r, float tmax)
{
    float d;
    for (int i = spheresCount; i >= 0; i--)
        d = is_intersecting(r, spheres[i]);
        if (bool(d) && d < tmax)
        {
            return true;
        }
    return false;
}

float inv_density_func()
{
    return 4.f * M_PI * spheres[light_index].rad * spheres[light_index].rad; //Sphere
}

float sphere_dispertion_func()
{
    return 4.f * M_PI * spheres[light_index].rad * spheres[light_index].rad * 2.f * M_PI;
}

vec3 radiance(in const Ray r) {
    int id;
    float t;
    if (!intersect(r, t, id)) {
        return vec3(0, 0, 0);
    }
    Sphere sphere = spheres[id];
    vec3 result = vec3(0, 0, 0);
    vec3 intersectionPoint = r.o + r.d * t;
    vec3 rayDirection = get_light_point(intersectionPoint) - intersectionPoint;
    float dirLength = length(rayDirection);
    rayDirection = normalize(rayDirection);
    //Fixing acnea
    vec3 offset = rayDirection * 0.15;
    //Correct lighting
    //result = sphere.c * light_color;
    float visibility = 1.0;
    if (occlude(Ray(intersectionPoint + offset, rayDirection), dirLength - 2.0 * length(offset))) {
        //result = Vec(0, 0, 0);
        visibility = 0.f;
    }
    
    vec3 normal = normalize((intersectionPoint - sphere.p));

    float n_dot_ray = dot(normal, rayDirection);
    if (n_dot_ray * dot(normal, r.d) < 0)
    {
        result = result + sphere.c * spheres[light_index].e / (sphere_dispertion_func()) * (abs(n_dot_ray) / ((dirLength * dirLength) * M_PI)) * visibility * inv_density_func();
    }
    return result;
}

layout( binding= 0, rgba8 ) uniform image2D framebuffer;

layout( local_size_x= 4, local_size_y=4 ) in;

void main( )
{
    Ray cam = Ray (cam_pos, normalize(cam_dir));  // cam pos, dir
    vec3 cx = vec3 (width * .5135 / height);
    vec3 cy = normalize(cross(cx, cam.d)) * .5135;
    
    float perc_y = 0.f;
    int x = int(gl_GlobalInvocationID.x);
    int y = int(gl_GlobalInvocationID.y);

    if(x >= 0 && y >= 0 && x < width && y < height) {
        vec3 result = vec3(0, 0, 0);
        vec3 r;
        for (int sy = 0, i = (height - y - 1) * width + x; sy < 2; sy++) { // 2x2 subpixel rows
            for (int sx = 0; sx < 2; sx++, r = vec3 (0, 0, 0)) { // 2x2 subpixel cols
                for (int s = 0; s < samps; s++)
                {
                    float dx = 0;
                    float dy = 0;

                    vec3 d =
                        cx * (((sx + .5 + dx) / 2 + x) / width - .5) +
                        cy * (((sy + .5 + dy) / 2 + y) / height - .5) + cam.d;
                    r =
                        r + radiance (Ray (cam.o + d * 140, normalize(d))) * (1. / samps);
                }       // Camera rays are pushed ^^^^^ forward to start in interior
                result = result + vec3 (clamp (r.x, 0, 1) * .25, clamp (r.y, 0, 1) * .25, clamp (r.z, 0, 1) * .25);
            }
        }
        barrier();
        imageStore(framebuffer, ivec2(x, y), vec4 (result.x, result.y, result.z, 1));
    }
}

#endif
