
// affiche un cube texture en 2 passes : la premiere passe remplit la texture, et la 2ieme passe dessine le cube en utilisant la texture de la premiere etape. utilise un framebuffer object.

#include <stdarg.h>
#include <algorithm>
#include "App.h"
#include "Widgets/nvSDLContext.h"

#include "Mesh.h"
#include "MeshIO.h"

#include "Image.h"
#include "ImageIO.h"

#include "GL/GLQuery.h"
#include "GL/GLTexture.h"
#include "GL/GLSampler.h"
#include "GL/GLFramebuffer.h"
#include "GL/GLBasicMesh.h"
#include "ProgramManager.h"

//! classe utilitaire : permet de construire une chaine de caracteres formatee. cf sprintf.
struct Format
{
	char text[1024];

	Format(const char *_format, ...)
	{
		text[0] = 0;     // chaine vide

		// recupere la liste d'arguments supplementaires
		va_list args;
		va_start(args, _format);
		vsnprintf(text, sizeof(text), _format, args);
		va_end(args);
	}

	~Format() {}

	// conversion implicite de l'objet en chaine de caracteres stantard
	operator const char *()
	{
		return text;
	}
};


static const gk::Vec4 light_color = gk::Vec4(5, 5, 5, 1);
static gk::Vec3 light_pos = gk::Vec3(0, 20, 0);
static gk::Point light_point = gk::Point(light_pos.x, light_pos.y, light_pos.z);
static const gk::Vec3 camera_pos = gk::Vec3(0, 20, 20);
static const gk::Point camera_point = gk::Point(camera_pos.x, camera_pos.y, camera_pos.z);
static const float shininess = 1;
static const int framebuffer_size = 128;

//! squelette d'application gKit.
class TP : public gk::App
{
	nv::SdlContext m_widgets;

	gk::GLProgram *m_program_depth;
	gk::GLProgram *m_program;
	gk::GLBasicMesh *m_mesh;
	gk::GLBasicMesh *m_mesh2;
	gk::GLBasicMesh *m_quad;
	gk::GLFramebuffer *m_framebuffer;
	gk::GLTexture *m_texture;
	gk::GLTexture *m_depth;
	gk::GLSampler *m_sampler;

	gk::GLCounter *m_time;
	gk::BBox bbox;
	GLuint color_texture;
	GLuint depth_texture;
	GLuint framebuffer;
	float light_angle;

public:
	// creation du contexte openGL et d'une fenetre
	TP()
		:
		gk::App()
	{
		// specifie le type de contexte openGL a creer :
		gk::AppSettings settings;
		settings.setGLVersion(3, 3);     // version 3.3
		settings.setGLCoreProfile();      // core profile
		settings.setGLDebugContext();     // version debug pour obtenir les messages d'erreur en cas de probleme

		// cree le contexte et une fenetre
		if (createWindow(512, 512, settings) < 0)
			closeWindow();

		m_widgets.init();
		m_widgets.reshape(windowWidth(), windowHeight());
	}

	~TP() {}

	int init()
	{
		light_angle = 0;
		// compilation simplifiee d'un shader program
		gk::programPath("shaders");
		// affichage du cube texture
		m_program = gk::createProgram("tp3.glsl");
		if (m_program == gk::GLProgram::null())
			return -1;

		// construction de la texture du cube (un damier noir et blanc)
		m_program_depth = gk::createProgram("tp3 depth.glsl");
		if (m_program_depth == gk::GLProgram::null())
			return -1;

		// charge un mesh
		gk::Mesh *mesh = gk::MeshIO::readOBJ("bigguy.obj");
		if (mesh == NULL)
			return -1;

		bbox = mesh->box;

		// cree le vertex array objet, description des attributs / associations aux variables du shader
		// utilise un gk::GLBasicMesh qui permet de creer et de configurer les buffers directement
		m_mesh = new gk::GLBasicMesh(GL_TRIANGLES, mesh->indices.size());
		m_mesh->createBuffer(m_program->attribute("position").location, mesh->positions);
		m_mesh->createBuffer(m_program->attribute("normal").location, mesh->normals);
		m_mesh->createIndexBuffer(mesh->indices);
		mesh->box;

		// mesh n'est plus necessaire, les donnees sont transferees dans les buffers sur la carte graphique
		delete mesh;

		// Charge le cube.
		mesh = gk::MeshIO::readOBJ("cube.obj");
		if (mesh == NULL)
			return -1;

		// cree le vertex array objet, description des attributs / associations aux variables du shader
		// utilise un gk::GLBasicMesh qui permet de creer et de configurer les buffers directement
		m_mesh2 = new gk::GLBasicMesh(GL_TRIANGLES, mesh->indices.size());
		m_mesh2->createBuffer(m_program->attribute("position").location, mesh->positions);
		m_mesh2->createBuffer(m_program->attribute("normal").location, mesh->normals);
		m_mesh2->createIndexBuffer(mesh->indices);

		// mesh n'est plus necessaire, les donnees sont transferees dans les buffers sur la carte graphique
		delete mesh;

		// nettoyage de l'etat opengl
		glBindVertexArray(0);                       // desactive le vertex array, cree par gk::GLBasicMesh
		glBindBuffer(GL_ARRAY_BUFFER, 0);           // desactive le buffer de positions
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);   // desactive le buffer d'indices

		// construit et configure un framebuffer object : la sortie 0 est associee a une texture couleur 128x128 (la texture est aussi cree).
		m_framebuffer = gk::createFramebuffer(GL_FRAMEBUFFER, 128, 128, gk::GLFramebuffer::COLOR0_BIT | gk::GLFramebuffer::DEPTH_BIT);
		// recupere la texture creee par le framebuffer
		m_texture = m_framebuffer->texture(gk::GLFramebuffer::DEPTH);

		// parametre un objet sampler pour acceder a la texture
		m_sampler = gk::createDepthSampler();

		// nettoyage
		glBindTexture(GL_TEXTURE_2D, 0);
		glBindFramebuffer(GL_FRAMEBUFFER, 0);

		// mesure du temps de dessin
		m_time = gk::createTimer();

		// ok, tout c'est bien passe
		return 0;
	}

	int quit()
	{
		delete m_mesh;
		return 0;
	}

	// a redefinir pour utiliser les widgets.
	void processWindowResize(SDL_WindowEvent& event)
	{
		m_widgets.reshape(event.data1, event.data2);
	}

	// a redefinir pour utiliser les widgets.
	void processMouseButtonEvent(SDL_MouseButtonEvent& event)
	{
		m_widgets.processMouseButtonEvent(event);
	}

	// a redefinir pour utiliser les widgets.
	void processMouseMotionEvent(SDL_MouseMotionEvent& event)
	{
		m_widgets.processMouseMotionEvent(event);
	}

	// a redefinir pour utiliser les widgets.
	void processKeyboardEvent(SDL_KeyboardEvent& event)
	{
		m_widgets.processKeyboardEvent(event);
	}

	int draw()
	{
		if (key(SDLK_ESCAPE))
			// fermer l'application si l'utilisateur appuie sur ESCAPE
			closeWindow();

		if (key('r'))
		{
			key('r') = 0;
			// recharge et recompile les shaders
			gk::reloadPrograms();
		}

		if (key('c'))
		{
			key('c') = 0;
			// enregistre l'image opengl
			gk::writeFramebuffer("screenshot.png");
		}

		// deplace et re-oriente le cube
		static float distance = 5;
		static float rotation = 45;

		if (key(SDLK_LEFT))
			light_point.x -= 1;
		if (key(SDLK_RIGHT))
			light_point.x += 1;

		if (key(SDLK_UP))
			light_point.z += 1;
		if (key(SDLK_DOWN))
			light_point.z -= 1;

		// change la taille du damier a chaque frame

		// mesurer le temps d'execution
		m_time->start();

		light_angle += 0.02f;
		light_pos.x = cos(light_angle) * 50;
		light_pos.z = sin(light_angle) * 50;
		light_point.x = light_pos.x;
		light_point.z = light_pos.z;
		// etape 1 : remplir la texture / le damier
		// activer le framebuffer configure pour dessiner dans la texture
		glBindFramebuffer(GL_DRAW_FRAMEBUFFER, m_framebuffer->name);
		// selectionne les sorties 
		glDrawBuffer(GL_COLOR_ATTACHMENT0);
		// configure la taille de l'image a dessiner
		glViewport(0, 0, m_framebuffer->width, m_framebuffer->height);
		// efface l'image avant de dessiner
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		// selectionne le shader program pour construire / dessiner le maillage
		glUseProgram(m_program_depth->name);
		// transformations
		gk::Transform model;
		gk::Transform mvp;
		draw2Bigguy(&model, &mvp);
		// Pdv de la source de lumiere
		gk::Point sphereCenter;
		float sphereRadius;
		bbox.BoundingSphere(sphereCenter, sphereRadius);
		auto dist = gk::Distance(light_point, sphereCenter);
		auto znear = dist - sphereRadius;
		auto zfar = dist + sphereRadius;
		auto fov = std::atan(sphereRadius / dist);
		auto p = gk::Perspective(gk::Degrees(fov), 1, znear, zfar);
		auto v = gk::LookAt(light_point, sphereCenter, gk::Vector(0, 1, 0));
		gk::Transform mvpLight = p * v * model;

		m_program_depth->uniform("mvpMatrix") = mvpLight.matrix();
		draw(mvpLight, m_mesh);

		//===================
		// Seconde partie:
		// nettoyage : reconfigure le pipeline pour dessiner dans la fenetre
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
		glEnable(GL_DEPTH_TEST);

		// efface l'image
		glViewport(0, 0, windowWidth(), windowHeight());
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		// dessiner quelquechose
		glUseProgram(m_program->name);

		// activer l'objet texture sur l'entree 0
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, m_texture->name);
		glGenerateMipmap(GL_TEXTURE_2D);
		// activer l'objet sampler sur l'entree 0
		glBindSampler(0, m_sampler->name);

		draw2Bigguy(&model, &mvp);
		draw2(mvp, mvpLight, model, m_mesh);
		//draw2Cube(&model, &mvp);
		//draw2(mvp, mvpLight, model, m_mesh2);

		//===================
		// Shadow map.
		glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
		glViewport(0, 0, windowWidth(), windowHeight());

		glBindFramebuffer(GL_READ_FRAMEBUFFER, m_framebuffer->name);
		glReadBuffer(GL_COLOR_ATTACHMENT0);
		glBlitFramebuffer(
			0, 0, m_framebuffer->width, m_framebuffer->height,
			0, 0, m_framebuffer->width, m_framebuffer->height,
			GL_COLOR_BUFFER_BIT, GL_NEAREST);
		glBindFramebuffer(GL_READ_FRAMEBUFFER, 0);

		// nettoyage
		glUseProgram(0);
		glBindVertexArray(0);
		glBindSampler(0, 0);
		glBindTexture(GL_TEXTURE_2D, 0);

		// mesurer le temps d'execution
		m_time->stop();

		// afficher le temps d'execution
		{
			m_widgets.begin();
			m_widgets.beginGroup(nv::GroupFlags_GrowDownFromLeft);

			m_widgets.doLabel(nv::Rect(), m_time->summary("draw").c_str());

			m_widgets.endGroup();
			m_widgets.end();
		}

		// afficher le dessin
		present();
		// continuer
		return 1;
	}

	void draw2Bigguy(gk::Transform *model, gk::Transform *mvp)
	{
		*model = gk::Translate(gk::Vector(0.f, 0.f, 0.f));
		gk::Transform view = gk::LookAt(camera_point, gk::Point(0, 0, 0), gk::Vector(0, 1, 0));
		gk::Transform projection = gk::Perspective(90, 1, 1, 1000);
		*mvp = projection * view * *model;
	}

	void draw2Cube(gk::Transform *model, gk::Transform *mvp)
	{
		*model = gk::Translate(gk::Vector(-50, -120, -100)) * gk::Scale(100);
		gk::Transform view = gk::LookAt(camera_point, gk::Point(0, 0, 0), gk::Vector(0, 1, 0));
		gk::Transform projection = gk::Perspective(90, 1, 1, 1000);
		*mvp = projection * view * *model;
	}

	void draw(gk::Transform mvpLight, gk::GLBasicMesh *mesh)
	{
		// parametrer le shader
		m_program->uniform("mvpMatrix") = mvpLight.matrix();

		// utilise l'utilitaire draw de gk::GLBasicMesh
		mesh->draw();
	}

	void draw2(gk::Transform mvp, gk::Transform mvpLight, gk::Transform model, gk::GLBasicMesh *mesh)
	{
		// parametrer le shader
		m_program->uniform("mvpMatrix") = mvp.matrix();      // transformation model view projection
		m_program->uniform("mvpMatrixLight") = mvpLight.matrix();
		m_program->uniform("model_inv") = model.inverse().matrix();
		m_program->uniform("light_pos") = light_pos;
		m_program->uniform("camera_pos") = camera_pos;
		m_program->uniform("diffuse_color") = gk::VecColor(1, 1, 1);     // couleur des fragments
		m_program->uniform("e") = light_color;
		m_program->uniform("m") = shininess;

		// utilise l'utilitaire draw de gk::GLBasicMesh
		mesh->draw();
	}
};

int main(int argc, char **argv)
{
	TP app;
	app.run();

	return 0;
}

