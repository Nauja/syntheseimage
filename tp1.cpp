#include "Vec.h"      // type vecteur, point, couleur, etc. de "base"
#include "Image.h"    // classe image
#include "ImageIO.h"  // entrees / sorties sur des images
#include "Triangle.h"

int dot(gk::Vec3 &v1, gk::Vec3 &v2)
{
	return v1.x * v2.x + v1.y * v2.y + v1.z * v2.z;
}

bool estDansTriangle1(int x, int y, gk::Triangle &triangle, gk::Vec3 &v0, gk::Vec3 &v1)
{
	gk::Vec3 v2 = gk::Point(x, y) - triangle.a;

	// Compute dot products
	float dot00 = dot(v0, v0);
	float dot01 = dot(v0, v1);
	float dot02 = dot(v0, v2);
	float dot11 = dot(v1, v1);
	float dot12 = dot(v1, v2);

	// Compute barycentric coordinates
	float invDenom = 1.0f / (dot00 * dot11 - dot01 * dot01);
	float u = (dot11 * dot02 - dot01 * dot12) * invDenom;
	float v = (dot00 * dot12 - dot01 * dot02) * invDenom;

	// Check if point is in triangle
	return (u >= 0) && (v >= 0) && (u + v < 1);
}

/**
 * Version fragmentation.
 */
void methode1(gk::Triangle &triangle, gk::Image &image)
{
	gk::Vec3 v0 = triangle.c - triangle.a;
	gk::Vec3 v1 = triangle.b - triangle.a;

    for(int y= 0; y < image.height; y++)    // chaque ligne
        for(int x= 0; x < image.width; x++)    // chaque colonne
			if(estDansTriangle1(x, y, triangle, v0, v1)) {
				image.setPixel(x, y, gk::VecColor(1, 0, 0));
			} else {
				image.setPixel(x, y, gk::VecColor(0, 0, 0));
			}
}

/**
 * Version subdivision.
 */
void methode2(gk::Triangle &triangle, gk::Image &image)
{
	// Si plus petit qu'un pixel.
	gk::BBox bbox = triangle.bbox();
	if((bbox.pMax.x - bbox.pMin.x ) <= 1 && (bbox.pMax.y - bbox.pMin.y) <= 1) {
		image.setPixel(bbox.pMax.x, bbox.pMax.y, gk::VecColor(1, 0, 0));
	} else {
		// Diviser.
		gk::Point p1 = triangle.a + (triangle.b - triangle.a) / 2;
		gk::Point p2 = triangle.a + (triangle.c - triangle.a) / 2;
		gk::Point p3 = triangle.b + (triangle.c - triangle.b) / 2;
		methode2(gk::Triangle(triangle.a, p1, p2), image);
		methode2(gk::Triangle(triangle.b, p1, p3), image);
		methode2(gk::Triangle(triangle.c, p2, p3), image);
		methode2(gk::Triangle(p1, p2, p3), image);
	}
}

bool estDansTriangle3(int x, int y, gk::Triangle &triangle)
{
	gk::Ray ray(gk::Point(x, y, 1), gk::Point(x, y, -1));
	float rt, ru, rv;
	return triangle.Intersect(ray, ray.tmax, rt, ru, rv);
}

/**
 * Version lancer de rayon.
 */
void methode3(gk::Triangle &triangle, gk::Image &image)
{
    for(int y= 0; y < image.height; y++)    // chaque ligne
        for(int x= 0; x < image.width; x++)    // chaque colonne
			if(estDansTriangle3(x, y, triangle)) {
				image.setPixel(x, y, gk::VecColor(1, 0, 0));
			} else {
				image.setPixel(x, y, gk::VecColor(0, 0, 0));
			}
}

int main( )
{
    gk::Image *image= gk::createImage(512, 512);    // cree une image de 512x512 pixels

	gk::Point p1(128, 128);
	gk::Point p2(384, 256);
	gk::Point p3(256, 384);
	gk::Triangle triangle(p1, p2, p3);

	methode2(triangle, *image);

    // enregistre le resultat
    gk::ImageIO::writeImage("out.bmp", image);
    
    delete image;
    return 0;
}