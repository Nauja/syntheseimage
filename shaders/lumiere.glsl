#version 430
#define M_PI 3.14159265359
#define DIFFUSE 0.3

#ifdef VERTEX_SHADER
    uniform mat4 mvpMatrix;
    layout (location= 0) in vec3 position;
	in vec3 normal;
	
	out vec3 p;	// Point p
	out vec3 n; // Normale du point p

    void main( )
    {
        gl_Position= mvpMatrix * vec4(position, 1.0);
        p= position;
		n= normal;
    }
#endif

#ifdef FRAGMENT_SHADER
    uniform vec4 diffuse_color;
	uniform mat4 model_inv;
	uniform vec3 light_pos;
	uniform vec3 camera_pos;
    out vec4 fragment_color;
	
	in vec3 p;	// Point p
	in vec3 n;	// Normale du point p
	
	uniform float m; // Shininess.
	uniform vec4 e; // Couleur lampe.
	
	float diffuse() {
		return DIFFUSE / M_PI;
	}

    void main( )
    {		
		vec3 s= (model_inv * vec4(light_pos, 1.0)).xyz;
		vec3 o= (model_inv * vec4(camera_pos, 1.0)).xyz;
		vec3 oV = normalize(o - p);
		vec3 l = normalize(s - p);
		vec3 h = normalize(l + oV);
		float cosTheta = max(0, dot(normalize(n), l));
		float cosThetaH = max(0, dot(normalize(n), h));
		float fr = pow(cosThetaH, m) * (m + 1) / (2 * M_PI);
		
        fragment_color = e * diffuse_color * cosTheta * (diffuse() + (1 - DIFFUSE) * fr);
        //~ fragment_color.rgb= diffuse_color.rgb;
        //~ fragment_color.rgb= abs(n.zzz);
    }
#endif

