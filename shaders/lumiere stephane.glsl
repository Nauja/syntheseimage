#version 430
#define DIFFUSE 0.3
#define PI 3.14159265358979323846

#ifdef VERTEX_SHADER
    uniform mat4 mvpMatrix;
    layout (location= 0) in vec3 position;
    layout (location= 1) in vec3 normal;
    out vec3 vertex_normal;
    out vec3 vertex_position;

    void main( )
    {
        gl_Position= mvpMatrix * vec4(position, 1.0);
        vertex_position= position;
        vertex_normal = normal;
        
    }
#endif

#ifdef FRAGMENT_SHADER
    
    uniform float m;
    uniform mat4 modelInv;
    uniform vec4 light_color;
    uniform vec4 diffuse_color;
    uniform vec3 camera_position;
    uniform vec3 light_position;
    in vec3 vertex_position;
    in vec3 vertex_normal;
    out vec4 fragment_color;

    float fr()
    {
        return DIFFUSE/PI;
    }

    float frspec( vec3 n, vec3 l, vec3 o)
    {
        vec3 h = normalize(l+o);
        float cosThetah = max(0,dot(n,h));
        return ((m+1)/(2*PI))*pow(cosThetah,m);
    }

    void main( )
    {

        vec3 local_cam = (modelInv *vec4(camera_position,1.0)).xyz;
        vec3 local_light = (modelInv *vec4(light_position,1.0)).xyz;
        vec3 n = normalize(vertex_normal);
        vec3 ln = local_light - vertex_position;
        vec3 on = local_cam - vertex_position;
        vec3 l = normalize(ln);
        vec3 o = normalize(on);

        float cosTheta = max(0,dot(n,l));
        fragment_color = light_color * diffuse_color * cosTheta *(fr() + frspec(n,l,o) *(1-DIFFUSE));
        //fragment_color.rgb= fr()*cosTheta*diffuse_color.rgb  ;
      // fragment_color.rgb= frspec(p,n,l,o)*diffuse_color.rgb;
        //~ fragment_color.rgb= diffuse_color.rgb;
        //~ fragment_color.rgb= abs(n.zzz);
    }
#endif

