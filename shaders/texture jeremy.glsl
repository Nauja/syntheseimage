#version 430
#define M_PI 3.14159265359
#define DIFFUSE 0.3

#ifdef VERTEX_SHADER
    uniform mat4 mvpMatrix;
    uniform mat4 normalMatrix;
    uniform mat4 mvpMatrixLight;
    
    layout(location= 0) in vec3 position;           //!< attribut
    layout(location= 1) in vec3 normal;             //!< attribut
    layout(location= 2) in vec3 texcoord;           //!< attribut
    
    out vec3 vertex_position_camera;
    out vec4 vertex_position_light;
    out vec3 vertex_normal;
    out vec2 vertex_texcoord;
    
    void main( )
    {
        gl_Position= mvpMatrix * vec4(position, 1.0);
        vertex_position_camera= position;
        vertex_position_light= mvpMatrixLight * vec4(position, 1.0);
        vertex_normal= mat3(normalMatrix) * normal;
        vertex_texcoord= texcoord.st;
    }
#endif

#ifdef FRAGMENT_SHADER
    uniform vec4 diffuse_color;
    uniform mat4 model_inv;
    uniform vec3 light_pos;
    uniform vec3 camera_pos;
    uniform sampler2D shadowMap;
    uniform sampler2D diffuse_texture;
    
    in vec3 vertex_position_camera; // Point p
    in vec4 vertex_position_light;
    in vec3 vertex_normal;
    in vec2 vertex_texcoord;

    uniform float m; // Shininess.
    uniform vec4 e; // Couleur lampe.
    
    out vec4 fragment_color;

    void main( )
    {
        vec3 s= (model_inv * vec4(light_pos, 1.0)).xyz;
        vec3 o= (model_inv * vec4(camera_pos, 1.0)).xyz;
        vec3 oV = normalize(o - vertex_position_camera);
        vec3 l = normalize(s - vertex_position_camera);
        vec3 h = normalize(l + oV);
        float cosTheta = max(0, dot(normalize(vertex_normal), l));
        float cosThetaH = max(0, dot(normalize(vertex_normal), h));
        float fr = pow(cosThetaH, m) * (m + 1) / (2 * M_PI);

float c = texture(diffuse_texture, vertex_texcoord).z;
    vec4 diffuse= vec4(c, c, c, 1);
    if(texture(diffuse_texture, vertex_texcoord).r > 0)    
        fragment_color = e * diffuse_color * cosTheta * (DIFFUSE / M_PI + (1 - DIFFUSE) * fr) * diffuse;
        else
        fragment_color = vec4(1, 1, 1, 1);
    }
#endif

