#version 430
#define M_PI 3.14159265359
#define DIFFUSE 0.3

#ifdef VERTEX_SHADER
    uniform mat4 mvpMatrix;
    uniform mat4 mvpMatrixLight;
    layout (location= 0) in vec3 position;
	layout (location= 1) in vec3 normal;
	
	out vec3 vertex_position_camera;	// Point p
	out vec4 vertex_position_light;
	out vec3 vertex_normal; // Normale du point p

    void main( )
    {
        gl_Position= mvpMatrix * vec4(position, 1.0);
        vertex_position_camera= position;
        vertex_position_light= mvpMatrixLight * vec4(position, 1.0);
		vertex_normal= normal;
    }
#endif

#ifdef FRAGMENT_SHADER
    uniform vec4 diffuse_color;
	uniform mat4 model_inv;
	uniform vec3 light_pos;
	uniform vec3 camera_pos;
	uniform sampler2D shadowMap;
    out vec4 fragment_color;
	
	in vec3 vertex_position_camera;	// Point p
	in vec4 vertex_position_light;
	in vec3 vertex_normal;	// Normale du point p
	
	uniform float m; // Shininess.
	uniform vec4 e; // Couleur lampe.
	
	float diffuse() {
		return DIFFUSE / M_PI;
	}

    void main( )
    {		
		vec3 s= (model_inv * vec4(light_pos, 1.0)).xyz;
		vec3 o= (model_inv * vec4(camera_pos, 1.0)).xyz;
		vec3 oV = normalize(o - vertex_position_camera);
		vec3 l = normalize(s - vertex_position_camera);
		vec3 h = normalize(l + oV);
		float cosTheta = max(0, dot(normalize(vertex_normal), l));
		float cosThetaH = max(0, dot(normalize(vertex_normal), h));
		float fr = pow(cosThetaH, m) * (m + 1) / (2 * M_PI);

		vec4 texcoord = (vertex_position_light / vertex_position_light.w) * 0.5 + 0.5;

		// Suppression des imperfections.
		float bias = 0.05*tan(acos(cosTheta));
		bias = clamp(bias, 0.0, 0.1);

		fragment_color = e * diffuse_color * cosTheta * (diffuse() + (1 - DIFFUSE) * fr);
		if (texcoord.z - bias > texture(shadowMap, texcoord.xy).x) {
			fragment_color *= 0.125f;
		}
    }
#endif

