#version 430

#ifdef VERTEX_SHADER
    uniform mat4 mvpMatrix;
    layout (location= 0) in vec3 position;

    void main( )
    {
        gl_Position= mvpMatrix * vec4(position, 1.0);
    }
#endif

#ifdef FRAGMENT_SHADER
    out vec4 frag_color;

    void main( )
    {
        frag_color = vec4(gl_FragCoord.zzz, 1);
    }
#endif

