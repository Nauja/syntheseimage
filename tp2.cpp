
#include "App.h"
#include "Widgets/nvSDLContext.h"

#include "Mesh.h"
#include "MeshIO.h"

#include "GL/GLQuery.h"
#include "GL/GLTexture.h"
#include "GL/GLBuffer.h"
#include "GL/GLVertexArray.h"
#include "ProgramManager.h"

#define SIZE 1

//! classe utilitaire : permet de construire une chaine de caracteres formatee. cf sprintf.
struct Format
{
    char text[1024];
    
    Format( const char *_format, ... )
    {
        text[0]= 0;     // chaine vide
        
        // recupere la liste d'arguments supplementaires
        va_list args;
        va_start(args, _format);
        vsnprintf(text, sizeof(text), _format, args);
        va_end(args);
    }
    
    ~Format( ) {}
    
    // conversion implicite de l'objet en chaine de caracteres stantard
    operator const char *( )
    {
        return text;
    }
};


//! squelette d'application gKit.
class TP : public gk::App
{
    nv::SdlContext m_widgets;
    
    gk::GLProgram *m_program;
    gk::GLVertexArray *m_vao;
    
    gk::GLBuffer *m_vertex_buffer;
	gk::GLBuffer *m_normal_buffer;
    gk::GLBuffer *m_index_buffer;
    int m_indices_size;
	int m_vertices_size;
    
    gk::GLCounter *m_time;

	float dX, dY, dZ;
			
	gk::Transform view;
	gk::Transform projection;
	gk::Transform viewport;

	gk::Vec3 light_pos;
	gk::Vec4 light_color;
	gk::Vec3 camera_pos;
	float shininess;
	float angle;
    
public:
    // creation du contexte openGL et d'une fenetre
    TP( )
        :
        gk::App(), dX(0), dY(0), dZ(0)
    {
        // specifie le type de contexte openGL a creer :
        gk::AppSettings settings;
        settings.setGLVersion(3,3);     // version 3.3
        settings.setGLCoreProfile();      // core profile
        settings.setGLDebugContext();     // version debug pour obtenir les messages d'erreur en cas de probleme
        
        // cree le contexte et une fenetre
        if(createWindow(512, 512, settings) < 0)
            closeWindow();
        
        m_widgets.init();
        m_widgets.reshape(windowWidth(), windowHeight());
		projection= gk::Perspective(50.f, 1.f, 1.f, 1000.f);
		viewport= gk::Viewport(windowWidth(), windowHeight());
    }
    
    ~TP( ) {}
    
    int init( )
    {
		light_color = gk::Vec4(1, 1, 1, 1);
		light_pos = gk::Vec3(0, 0, 0);
		camera_pos = gk::Vec3(0, 0, 0);
		shininess = 35;
		angle = 0;

        // compilation simplifiee d'un shader program
        gk::programPath("shaders");
        m_program= gk::createProgram("lumiere.glsl");
        if(m_program == gk::GLProgram::null())
            return -1;
        
        // charge un mesh
        gk::Mesh *mesh= gk::MeshIO::readOBJ("bigguy.obj");
        if(mesh == NULL)
            return -1;
        
        // cree le vertex array objet, description des attributs / associations aux variables du shader
        m_vao= gk::createVertexArray();

        // cree le buffer de position
		std::vector<gk::Vec3> positions;
		std::vector<gk::Vec3> normals;
		std::vector<GLuint> indices;
		
		for(int i = 0; i < SIZE; ++i) {
			positions.insert(positions.end(), mesh->positions.begin(), mesh->positions.end());
			normals.insert(normals.end(), mesh->normals.begin(), mesh->normals.end());
        
			// cree le buffer d'indices et l'associe au vertex array
			indices.insert(indices.end(), mesh->indices.begin(), mesh->indices.end());
			// conserve le nombre d'indices (necessaire pour utiliser glDrawElements)
		}
		normals = positions;

		m_vertex_buffer= gk::createBuffer(GL_ARRAY_BUFFER, positions);
		m_normal_buffer= gk::createBuffer(GL_ARRAY_BUFFER, normals);
		// associe le contenu du buffer a la variable 'position' du shader
		glVertexAttribPointer(m_program->attribute("position"), 3, GL_FLOAT, GL_FALSE, 0, 0);
		glVertexAttribPointer(m_program->attribute("normal"), 3, GL_FLOAT, GL_FALSE, 0, 0);
		// active l'utilisation du buffer 
		glEnableVertexAttribArray(m_program->attribute("position"));
		glEnableVertexAttribArray(m_program->attribute("normal"));
		m_index_buffer= gk::createBuffer(GL_ELEMENT_ARRAY_BUFFER, indices);
		m_indices_size = mesh->indices.size();
		m_vertices_size = mesh->positions.size();
        
        // mesh n'est plus necessaire, les donnees sont transferees dans les buffers sur la carte graphique
        delete mesh;
        
        // nettoyage de l'etat opengl
        glBindVertexArray(0);   // desactive le vertex array
        glBindBuffer(GL_ARRAY_BUFFER, 0);       // desactive le buffer de positions
        glBindBuffer(GL_ARRAY_BUFFER, 0);       // desactive le buffer de positions
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);       // desactive le buffer d'indices
        
        // mesure du temps de dessin
        m_time= gk::createTimer();
        
        // ok, tout c'est bien passe
        return 0;
    }
    
    int quit( )
    {
        return 0;
    }

    // a redefinir pour utiliser les widgets.
    void processWindowResize( SDL_WindowEvent& event )
    {
        m_widgets.reshape(event.data1, event.data2);
    }
    
    // a redefinir pour utiliser les widgets.
    void processMouseButtonEvent( SDL_MouseButtonEvent& event )
    {
        m_widgets.processMouseButtonEvent(event);
    }
    
    // a redefinir pour utiliser les widgets.
    void processMouseMotionEvent( SDL_MouseMotionEvent& event )
    {
        m_widgets.processMouseMotionEvent(event);
    }
    
    // a redefinir pour utiliser les widgets.
    void processKeyboardEvent( SDL_KeyboardEvent& event )
    {
        m_widgets.processKeyboardEvent(event);
    }
    
    int draw( )
    {
        if(key(SDLK_ESCAPE))
            // fermer l'application si l'utilisateur appuie sur ESCAPE
            closeWindow();
        
        if(key('r'))
        {
            key('r')= 0;
            // recharge et recompile les shaders
            gk::reloadPrograms();
        }
        
        if(key('c'))
        {
            key('c')= 0;
            // enregistre l'image opengl
            gk::writeFramebuffer("screenshot.png");
        }

		float speed = 0.5f;
		if(key(SDLK_LEFT))
		{
			dX -= speed;
		} else if(key(SDLK_RIGHT))
		{
			dX += speed;
		}
		if(key(SDLK_UP))
		{
			dY += speed;
		} else if(key(SDLK_DOWN))
		{
			dY -= speed;
		}
        
        //
        glViewport(0, 0, windowWidth(), windowHeight());
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        
        // mesurer le temps d'execution
        m_time->start();
        
        /* configuration minimale du pipeline :
            glBindVertexArray() // association du contenu des buffers aux attributs declares dans le shader
            glUseProgram()  // indique quelle paire de shaders utiliser
            { ... } // parametrer les uniforms des shaders

            glDraw()    // execution du pipeline
         */

        // selectionner un ensemble de buffers et d'attributs de sommets
        glBindVertexArray(m_vao->name);

        // dessiner quelquechose
        glUseProgram(m_program->name);
		
		angle += 1;
		for(int i = 0; i < SIZE; ++i) {
			// transformations
			gk::Transform model =  gk::Translate( gk::Vector(10.0f * i + dX, 0.f + dY, -50 + dZ) ) * gk::Rotate(angle, gk::Vector(0, 1, 0));
        
			// composition des transformations
			gk::Transform mv= view * model;
			gk::Transform mvp= projection * mv;
			gk::Transform mvpv= viewport * mvp;
        
			// parametrer le shader
			m_program->uniform("mvpMatrix")= mvp.matrix();      // transformation model view projection
			m_program->uniform("model_inv")= model.inverse().matrix();
			m_program->uniform("light_pos")= light_pos;
			m_program->uniform("camera_pos")= camera_pos;
			m_program->uniform("diffuse_color")= gk::VecColor(1, 1, 1);     // couleur des fragments
			m_program->uniform("e")= light_color;
			m_program->uniform("m")= shininess;

			// dessiner un maillage indexe
			glDrawElementsBaseVertex(GL_TRIANGLES, m_indices_size, GL_UNSIGNED_INT, 0, (i == 0 ? 0 : m_vertices_size));
		}

        // nettoyage
        glUseProgram(0);
        glBindVertexArray(0);
        
        // mesurer le temps d'execution
        m_time->stop();
        
        // afficher le temps d'execution
        {
            m_widgets.begin();
            m_widgets.beginGroup(nv::GroupFlags_GrowDownFromLeft);
            
            m_widgets.doLabel(nv::Rect(), m_time->summary("draw").c_str());
            
            m_widgets.endGroup();
            m_widgets.end();
        }
        
        // afficher le dessin
        present();
        // continuer
        return 1;
    }
};


int main( int argc, char **argv )
{
    TP app;
    app.run();
    
    return 0;
}

